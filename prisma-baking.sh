pipeline {
    agent any
    
    stages {
        stage("Ami-creation(Baking)") {
            steps {
                dir('CalibrationResults') {
                  git url: 'https://gitlab.com/NathanH05/prisma-baking.git'

                 ansiblePlaybook([
                  colorized: true,
                  credentialsId: '1',
                  installation: 'ansible',
                  playbook: 'prisma-baking.yml',
                  extras: "--extra-vars 'tooling_environment=dev aws_region=us-east-1 application_stack=blue app_role_name=prisma'"
                    ])
                }
            }
        }

    }
}
