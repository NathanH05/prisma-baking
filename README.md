# Prisma Baking

An Ansible Playbook to pull down the application code, build it and store it on an Amazon Machine Image. Easily looked up for consumption by ansible and terraform.


## Terraform cli
```terraform init```
```terraform apply```

## Ansible Parameters

### aws_region (mandatory)
The AWS region that we are accessing the AWS API's (as well as creating things in)

---

### tooling_environment (mandatory)
The tooling environment that is being used, e.g. `dev`, `qa`, `uat`, `prod`

### application_stack (optional)

---

Single node
```
---

- name: AWS Infrastructure
  hosts: localhost
  connection: local
  pre_tasks:
    - name: Validate parameters
      assert:
        that:
          - aws_region is defined
          - tooling_environment is defined
    - set_fact:
        application_role_name: prisma
  roles:
    - role: infra
      server:
        ebs_volume_size: 60
        subnet: 'a'
#        {{ lookup("max-ip-subnet") }}'

- hosts: "{{ groups['tag_roles_prisma'] }}"
  become: yes
  gather_facts: yes
  tasks:
    - name: Wait for system to become reachable again
      wait_for_connection:
        delay: 45
        sleep: 15
        timeout: 240
    - name: Validate parameters
      assert:
        that:
          - aws_region is defined
          - tooling_environment is defined
    - set_fact:
        application_role_name: prisma
  roles:
    - role: baking
      server:
        ebs_volume_size: 60
        subnet: 'a'
      instance_id: hostvars['localhost']['instance_ids']
#        {{ lookup("max-ip-subnet") }}'

- name: AWS Infrastructure
  hosts: localhost
  connection: local
  tasks:
    - name: Set iso8601_basic_short timestamp for the new ami.
      set_fact:
        ami_timestamp: '{{ ansible_date_time.iso8601_basic }}'
    - name: Ami bundling
      ec2_ami:
        instance_id: "{{hostvars['localhost']['ec2_instance_ids']|first}}"
        region: '{{ aws_region }}'
        state: present
        name: 'prisma-{{ ami_timestamp }}'
        wait: yes
        no_reboot: 'no'
        delete_snapshot: yes
        tags:
          Name: 'prisma-{{ ami_timestamp }}'
          controlled_by: ansible
          created_at: '{{ ansible_date_time.iso8601 }}'
          role: 'prisma'
          ami_type: 'pet'
          tooling_environment: '{{ tooling_environment }}'
          base_ami: 'origin_ami_id'
          encrypted: ''
          hel7x_image: ''
      register: new_ami

```


---


## CLI Usage

## Single node
```
ansible-playbook prisma-baking.yml --extra-vars "tooling_environment=${var.env} application_stack=dev aws_region=us-east-1"
```
---

note:
application_stack is optional. If not provided an available stack colour will be looked up

## Results
After the invocation of this role the following things will have happened:

* git is installed
* git cloned the application repository
* npm and next could also be installed here (currently installed in user data)

## Contributing

1. **Clone** the project to your own machine
2. **Commit** changes to your own branch
3. **Push** your work back up to your fork
4. Submit a **Pull request** so that we can review your changes
